#include <bits/stdc++.h>
#include "simpson.h"

using namespace std;

int main(){
  for (int i = 4; i < 50; i+=2){
    cout << setfill(' ') << setw(5) << i << " " << setprecision(32) << simpson([](double a){ return sin(a); }, i, 0, M_PI) << endl;
  }
}
