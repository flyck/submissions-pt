#include <bits/stdc++.h>
#include "simpson.h"

double simpson(std::function<double(double)> fun, int bins, double start, double end){
  assert(bins % 2 == 0);
  assert(bins > 2);
  double result = fun(start) + fun(end);
  for (int i = 1; i < bins; i++)
    result += (((i&1) + 1) << 1) * fun(i * (end-start) / bins + start);
  return result / 3 * (end - start) / bins;
}
