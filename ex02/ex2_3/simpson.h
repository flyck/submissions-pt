
//fun: std::function(double)->double, needs to be defined between start and end inclusive
//bins: int, number of bins to use for integration, needs to be even and greater than 2, this is asserted
//start: double, where to start the integration
//end: double, where to end the integration
//returns the simpson integral of fun within machine precision
//will not generate any exceptions, this is not assured for fun
//requires standard library for std::function
double simpson(std::function<double(double)> fun, int bins, double start, double end);
