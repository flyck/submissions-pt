#include <bits/stdc++.h>

using namespace std;

int main(){
  int n;
  cin >> n;
  vector<int> nums(n); //alternatively use: int nums[n];
  int sum = 0;
  for_each(nums.begin(), nums.end(), [&sum](int & i){ cin >> i; sum += i; });
  reverse(nums.begin(), nums.end());
  for_each(nums.begin(), nums.end(), [sum](int i){ cout << i / (double) sum << " "; });
  cout << endl;
}
