#include <bits/stdc++.h>
#include "simpson.h"

using namespace std;

int main(){
  cout << setprecision(32) << simpson([](double a){ return sin(a); }, 42, 0, M_PI) << endl;
}
